import datetime
import os
import re
import logging


class Window:
    def __init__(self, host, current, previous):
        self.host = host
        self.current = current
        self.end = current + datetime.timedelta(seconds=3600)
        self.count = 1
        self.previous = previous
        self.remove = False

    def add(self, current):
        if current < self.end:
            self.count += 1
        else:
            self.remove = True


class Blocked:
    def __init__(self, hostname, timestamp):
        self.hostname = hostname
        self.begin = timestamp
        self.count = 0
        self.blocked_time = datetime.timedelta(seconds=5*60)
        self.remove = False
        self.end = None

    def add(self, hostname, timestamp):
        if self.count <= 3:
            self.count += 1
            if self.count > 3:
                self.end = timestamp + self.blocked_time


class Top:
    def __init__(self, size=10):
        self.size = size
        self.data = {}

    def add(self, obj, count):
        if self.data and len(self.data) >= self.size:
            minimum = self.find_minimum()
            if (count > minimum[1] or
                (count == minimum[1] and
                 obj < minimum[0])
            ):
                self.data.pop(self.find_minimum()[0], None)
                self.data[obj] = count
        else:
            self.data[obj] = count

    def find_minimum(self):
        # if the list was larger, the minimum would probably be stored as it is added.
        # many interviews of the type keep track of minimum, but I have assumed this is a
        # small list
        if not self.data:
            return ('', 0)
        sort = self.sort()
        return sort[-1]

    def sort(self):
        return sorted(self.data.items(), key=lambda x: x[1], reverse=True)


class Log:

    def __init__(self, log):
        self.hostnames = {}
        self.resources = {}
        self.timestamps = {}
        self.windows = []
        self.previous = None
        self.top_windows = {}
        self.blocked = []
        self.log_blocked = []
        self.duration = datetime.timedelta(seconds=20)
        with open(log) as log:
            line = True
            while line:
                line = log.readline()
                self.valid(line)
        self.feature1()
        self.feature2()
        self.feature3()
        self.feature4()

    def add_hostname(self, hostname):
        try:
            self.hostnames[hostname] += 1
        except KeyError:
            self.hostnames[hostname] = 1

    def add_bandwidth(self, resource, size):
        if size == '-':
            size = 0
        try:
            self.resources[resource] += int(size)
        except KeyError:
            self.resources[resource] = int(size)

    def add_timestamp(self, host, current):
        for window in self.windows:
            window.add(current)
        if self.previous is None:
            self.previous = current - datetime.timedelta(seconds=1)
        win = Window(host, current, self.previous)
        self.previous = current
        self.windows.append(win)

        self.windows[:] = [window for window in self.windows if not window.remove]
        self.top_windows = {window.current: (window.previous, window.count) for window in self.windows}

    def add_blocked(self, hostname, current):
        self.blocked.append(Blocked(hostname, current))

    def checked_blocked(self, hostname, timestamp):
        end = None
        for blocked in self.blocked:
            if timestamp > blocked.begin + self.duration:
                blocked.remove = True
            else:
                if blocked.hostname == hostname:
                    blocked.add(hostname, timestamp)
                    if blocked.end:
                        if not end:
                            end = blocked.end
                        else:
                            end = max(blocked.end, end)
        self.blocked[:] = [blocked for blocked in self.blocked if not blocked.remove]
        return end

    def valid(self, line):
        #        self.valid = '199.72.81.55 - - [01/Jul/1995:00:00:01 -0400] "POST /login HTTP/1.0" 401 1420'
        if line:
            correct = re.compile(r'\A(.*?) - - \[(.*?) -0400\] '
                                 r'"(GET|POST|HEAD|POST|PUT|DELETE|TRACE|OPTIONS|CONNECT|PATCH) '
                                 r'(.*?)(| HTTP.*?)" (\d{3}) (.*)')
            matched = re.match(correct, line)
            if matched:
                hostname = matched.group(1)
                timestamp = matched.group(2)
#                request = matched.group(3)
                resource = matched.group(4)
#                version = matched.group(5)
                http = matched.group(6)
                num_bytes = matched.group(7)

                # could split this into a feature function, pending performance issues
                self.add_hostname(hostname)
                self.add_bandwidth(resource, num_bytes)

                current = datetime.datetime.strptime(timestamp, "%d/%b/%Y:%H:%M:%S")
                self.add_timestamp(hostname, current)

                if http == '401':
                    self.add_blocked(hostname, current)
                end = self.checked_blocked(hostname, current)
                if end and end > current:
                    self.log_blocked.append(line)
                # end of the features function

            else:
                logging.exception("Incorrect Log Entry: %s" % line)

    def feature1(self):
        top = Top()
        for hostname in self.hostnames:
            top.add(hostname, self.hostnames[hostname])
        with open(os.path.join('log_output', 'hosts.txt'), 'w') as hosts_text:
            for hostname in top.sort():
                hosts_text.write("%s,%d\n" % (hostname[0], hostname[1]))

    def feature2(self):
        top = Top()
        for resource in self.resources:
            top.add(resource, self.resources[resource])
        with open(os.path.join('log_output', 'resources.txt'), 'w') as resources_text:
            for resource in top.sort():
                resources_text.write("%s\n" % resource[0])

    def feature3(self):
        # made assumption that the log is sorted by timestamp
        # I will probably write this as a rolling window in time if I had time to do this again
        with open(os.path.join('log_output', 'hours.txt'), 'w') as hours_text:
            sort = sorted(self.top_windows.items(), key=lambda x: x[1][1], reverse=True)[0:10]
            count = 0
            for window in sort:
                for seconds in range(1, (window[0] - window[1][0]).seconds + 1):
                    string = window[1][0]+datetime.timedelta(seconds=seconds)
                    string = string.strftime("%d/%b/%Y:%H:%M:%S -0400")
                    hours_text.write("%s,%d\n" % (string, window[1][1]))
                    count += 1
                    if count == 10:
                        return

    def feature4(self):
        with open(os.path.join('log_output', 'blocked.txt'), 'w') as blocked_text:
            for item in self.log_blocked:
                blocked_text.write(item)
